package main

import (
	"github.com/gobuffalo/packr"
	"github.com/labstack/echo"
	"mime"
	"net/http"
	"os"
	"path"
	"time"
)

var (
	box packr.Box
)

func init() {
	box = packr.NewBox("../static")
	mime.AddExtensionType(".html", "text/html")
	mime.AddExtensionType(".htm", "text/htm")
	mime.AddExtensionType(".css", "text/css")
	mime.AddExtensionType(".js", "application/javascript")
	mime.AddExtensionType(".wasm", "application/wasm")
	mime.AddExtensionType(".pdf", "application/pdf")
	mime.AddExtensionType(".csv", "text/csv")
}

func static(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		p := c.Request().URL.Path
		f, ferr := box.Open(p)
		if os.IsNotExist(ferr) {
			return next(c)
		}
		if ferr != nil {
			return ferr
		}
		fi, ferr := f.Stat()
		if ferr != nil {
			return ferr
		}
		if fi.IsDir() {
			return next(c)
		}

		resp := c.Response()
		resp.Header().Set("Cache-Control", "public, max-age=900")
		http.ServeContent(resp, c.Request(), path.Base(p), time.Time{}, f)
		return nil
	}
}
