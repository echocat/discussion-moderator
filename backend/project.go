package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/labstack/echo"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

var (
	projectsRoot = flag.String("projects", "./var/projects", "Location where all modifiable resources are stored.")
)

var defaultProject = project{
	MaximumDurationPerPage:                    time.Minute * 2,
	WarmUpDurationPerPage:                     time.Second * 30,
	OnMaximumDurationPerPageReachedMoveToNext: true,
	OnWarmDurationPerPageReachedMoveToNext:    true,
}

type project struct {
	rootDirectory                             string
	Id                                        string        `json:"id"`
	Name                                      string        `json:"name"`
	MaximumDurationPerPage                    time.Duration `json:"maximumDurationPerPage"`
	WarmUpDurationPerPage                     time.Duration `json:"warmUpDurationPerPage"`
	OnMaximumDurationPerPageReachedMoveToNext bool          `json:"onMaximumDurationPerPageReachedMoveToNext"`
	OnWarmDurationPerPageReachedMoveToNext    bool          `json:"onWarmDurationPerPageReachedMoveToNext"`
}

func projects() (result []*project, err error) {
	result = []*project{}
	f, err := os.Open(*projectsRoot)
	if os.IsNotExist(err) {
		err = nil
		return
	}
	if err != nil {
		err = fmt.Errorf("could not read projects directory (%s): %v", *projectsRoot, err)
		return
	}
	fis, err := f.Readdir(-1)
	if err != nil {
		err = fmt.Errorf("could not read projects directory (%s): %v", *projectsRoot, err)
		return
	}
	for _, fi := range fis {
		id := fi.Name()
		candidate, cerr := newProjectBy(id)
		if os.IsNotExist(cerr) {
			continue
		}
		if cerr != nil {
			err = fmt.Errorf("could not read project directory (%s/%s): %v", *projectsRoot, id, cerr)
			return
		}
		result = append(result, candidate)
	}
	return
}

func newProjectBy(id string) (*project, error) {
	rootDirectory := projectRoot(id)
	fi, err := os.Stat(rootDirectory)
	if os.IsNotExist(err) {
		return nil, err
	}
	if err != nil {
		return nil, fmt.Errorf("could not open directory for project with id '%s': %v", id, err)
	}
	if !fi.IsDir() {
		return nil, os.ErrNotExist
	}
	return newProjectFrom(id, rootDirectory)
}

func newProjectFrom(id string, rootDirectory string) (result *project, err error) {
	v := defaultProject
	result = &v
	result.rootDirectory = rootDirectory
	result.Name = filepath.Base(rootDirectory)

	dfn := result.documentFile()
	fi, err := os.Stat(dfn)
	if os.IsNotExist(err) {
		return nil, err
	}
	if err != nil {
		return nil, fmt.Errorf("could not open directory for project with id '%s': %v", id, err)
	}
	if fi.IsDir() {
		return nil, os.ErrNotExist
	}

	if err = result.load(); err != nil {
		result.Id = id
		if os.IsNotExist(err) {
			err = nil
			result.save()
		}
		return
	}

	result.Id = id
	return
}

func (instance *project) load() error {
	fn := instance.projectFile()
	f, err := os.Open(fn)
	if os.IsNotExist(err) {
		return err
	}
	if err != nil {
		return fmt.Errorf("could not open project file (%s): %v", fn, err)
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(instance)
	if err != nil {
		return fmt.Errorf("could not read project file (%s): %v", fn, err)
	}

	return nil
}

func (instance *project) save() error {
	err := os.MkdirAll(instance.rootDirectory, 755)
	if err != nil {
		return fmt.Errorf("could not create root directory (%s): %v", instance.rootDirectory, err)
	}
	fn := instance.projectFile()
	f, err := os.OpenFile(fn, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("could not save project file (%s): %v", fn, err)
	}
	defer f.Close()

	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")

	err = encoder.Encode(instance)
	if err != nil {
		return fmt.Errorf("could not save project file (%s): %v", fn, err)
	}

	return nil
}

func (instance *project) openDocumentFile() (*os.File, error) {
	fn := instance.documentFile()
	f, err := os.Open(fn)
	if os.IsNotExist(err) {
		return nil, err
	}
	if err != nil {
		return nil, fmt.Errorf("could not open document file (%s): %v", fn, err)
	}
	return f, nil
}

func (instance *project) openStatisticsFile() (*os.File, error) {
	fn := instance.statisticsFile()
	f, err := os.Open(fn)
	if os.IsNotExist(err) {
		return nil, err
	}
	if err != nil {
		return nil, fmt.Errorf("could not open statistics file (%s): %v", fn, err)
	}
	return f, nil
}

func (instance *project) writeStatistics(record []string) error {
	err := os.MkdirAll(instance.rootDirectory, 755)
	if err != nil {
		return fmt.Errorf("could not create root directory (%s): %v", instance.rootDirectory, err)
	}
	fn := instance.statisticsFile()
	f, err := os.OpenFile(fn, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("could not open statistics file (%s): %v", fn, err)
	}
	defer f.Close()

	writer := csv.NewWriter(f)
	err = writer.Write(record)
	if err != nil {
		return fmt.Errorf("could not write statistics file (%s): %v", fn, err)
	}
	writer.Flush()

	return nil
}

func (instance *project) documentFile() string {
	return filepath.Join(instance.rootDirectory, "document.pdf")
}

func (instance *project) statisticsFile() string {
	return filepath.Join(instance.rootDirectory, "statistics.csv")
}

func (instance *project) projectFile() string {
	return filepath.Join(instance.rootDirectory, "project.json")
}

func projectRoot(id string) string {
	return filepath.Join(*projectsRoot, id)
}

func handleGetProjects(c echo.Context) error {
	pjs, err := projects()
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, pjs)
}

func handleGetProjectIndex(c echo.Context) error {
	id := c.Param("id")
	_, err := newProjectBy(id)
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	f, err := box.Open("project.html")
	if err != nil {
		return err
	}
	defer f.Close()
	return c.Stream(http.StatusOK, "text/html", f)
}

func handleGetProjectProperties(c echo.Context) error {
	id := c.Param("id")
	pj, err := newProjectBy(id)
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, pj)
}

func handleGetProjectDocument(c echo.Context) error {
	id := c.Param("id")
	pj, err := newProjectBy(id)
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	df, err := pj.openDocumentFile()
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	defer df.Close()

	c.Response().Header().Set("Cache-Control", "public, max-age=900")
	return c.Stream(http.StatusOK, "application/pdf", df)
}

func handleGetProjectStatistics(c echo.Context) error {
	id := c.Param("id")
	pj, err := newProjectBy(id)
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}
	df, err := pj.openStatisticsFile()
	if os.IsNotExist(err) {
		return c.Blob(http.StatusOK, "text/csv", []byte{})
	}
	if err != nil {
		return err
	}
	defer df.Close()

	c.Response().Header().Set("Cache-Control", "public, max-age=900")
	return c.Stream(http.StatusOK, "text/csv", df)
}

func handlePutProjectStatistics(c echo.Context) error {
	id := c.Param("id")
	pj, err := newProjectBy(id)
	if os.IsNotExist(err) {
		return c.String(http.StatusNotFound, "Not found")
	}
	if err != nil {
		return err
	}

	var record []string
	err = c.Bind(&record)
	if err != nil {
		return err
	}

	err = pj.writeStatistics(record)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
