module gitlab.com/echocat/discussion-moderator/backend/v1

require (
	github.com/gobuffalo/packr v1.13.5
	github.com/labstack/echo v0.0.0-20180911044237-1abaa3049251
	github.com/labstack/gommon v0.0.0-20180312174116-6fe1405d73ec
)
