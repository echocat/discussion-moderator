package main

import (
	"flag"
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"net/http"
	"regexp"
)

var (
	listen            = flag.String("listen", ":8080", "Listen to this address.")
	projectPathRegexp = regexp.MustCompile("^/projects/([a-zA-Z0-9._\\-]+)$")
)

func main() {
	flag.Parse()

	e := echo.New()
	e.HTTPErrorHandler = httpErrorHandler
	e.HideBanner = true
	e.HidePort = true
	e.Logger.SetLevel(log.INFO)
	e.Use(middleware.Logger())
	e.Use(middleware.RequestID())
	e.Use(middleware.CORS())
	e.Use(fixProjectPath)
	e.Use(static)

	e.GET("/", handleGetIndex)
	e.GET("/projects", handleGetProjects)
	e.GET("/projects.json", handleGetProjects)
	e.GET("/projects/:id/", handleGetProjectIndex)
	e.GET("/projects/:id/properties", handleGetProjectProperties)
	e.GET("/projects/:id/properties.json", handleGetProjectProperties)
	e.GET("/projects/:id/document.pdf", handleGetProjectDocument)
	e.GET("/projects/:id/document", handleGetProjectDocument)
	e.GET("/projects/:id/statistics.csv", handleGetProjectStatistics)
	e.GET("/projects/:id/statistics", handleGetProjectStatistics)
	e.PUT("/projects/:id/statistics.csv", handlePutProjectStatistics)
	e.PUT("/projects/:id/statistics", handlePutProjectStatistics)

	e.Logger.Fatal(e.Start(*listen))
}

func handleGetIndex(c echo.Context) error {
	f, err := box.Open("index.html")
	if err != nil {
		return err
	}
	defer f.Close()
	return c.Stream(http.StatusOK, "text/html", f)
}

func fixProjectPath(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()
		p := req.URL.Path

		match := projectPathRegexp.FindStringSubmatch(p)
		if match != nil {
			if req.Method == "GET" {
				return c.Redirect(http.StatusPermanentRedirect, fmt.Sprintf("/projects/%s/", match[1]))
			} else {
				return c.String(http.StatusBadRequest, "Bad request")
			}
		}

		return next(c)
	}
}

func httpErrorHandler(err error, c echo.Context) {
	var (
		code = http.StatusInternalServerError
		msg  interface{}
	)

	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		msg = he.Message
		if he.Internal != nil {
			msg = fmt.Sprintf("%v, %v", err, he.Internal)
		}
	} else {
		msg = http.StatusText(code)
	}
	if _, ok := msg.(string); ok {
		msg = echo.Map{"message": msg}
	}

	if !c.Response().Committed {
		if c.Request().Method == echo.HEAD {
			err = c.NoContent(code)
		} else {
			err = c.JSON(code, msg)
		}
		if err != nil {
			c.Logger().Error(err)
		}
	}
}
