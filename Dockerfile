FROM golang:1.11 AS builder

RUN go get -u github.com/gobuffalo/packr/...

COPY . /src

RUN cd /src/frontend \
    && CGO_ENABLED=0 GOOS=js GOARCH=wasm go build -a -installsuffix nocgo -o /src/static/app.wasm ./...
RUN cd /src/backend \
    && packr clean && packr -z \
    && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix nocgo -o /app ./...

FROM scratch
WORKDIR /
COPY --from=builder /app /app
ENTRYPOINT ["/app"]
