package main

import (
	"fmt"
	"net/url"
	"syscall/js"
	"time"
)

var (
	document = js.Global().Get("document")
)

func getElementById(id string) (element js.Value, ok bool) {
	element = document.Call("getElementById", id)
	ok = element != js.Null() && element != js.Undefined()
	return
}

func hashArguments() (result url.Values) {
	result = url.Values{}
	plain := js.Global().Get("location").Get("hash").String()
	if len(plain) <= 1 || plain[0] != '#' {
		return
	}
	var err error
	if result, err = url.ParseQuery(plain[1:]); err != nil {
		warnf("Cannot parse provided hash: %v", err)
		return
	}
	return
}

func formatDuration(d time.Duration) string {
	d = d.Round(time.Second)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	d -= m * time.Minute
	s := d / time.Second
	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
}

func addClassToElement(element js.Value, name string) {
	element.Get("classList").Call("add", name)
}

func removeClassFromElement(element js.Value, name string) {
	element.Get("classList").Call("remove", name)
}

func showElement(element js.Value) {
	removeClassFromElement(element, "hidden")
}

func hideElement(element js.Value) {
	addClassToElement(element, "hidden")
}

func timeoutPromise(duration time.Duration) (promise js.Value) {
	global := js.Global()
	return global.Get("Promise").New(js.NewCallback(func(args []js.Value) {
		resolve := args[0]
		global.Call("setTimeout", resolve.Call("bind", nil, nil), int64(duration/time.Millisecond))
	}))
}
