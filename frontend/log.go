package main

import (
	"fmt"
	"syscall/js"
)

var (
	console = js.Global().Get("console")
)

func warnf(format string, args ...interface{}) {
	_log("warn", format, args...)
}

func errorf(format string, args ...interface{}) {
	_log("error", format, args...)
}

func infof(format string, args ...interface{}) {
	_log("log", format, args...)
}

func debugf(format string, args ...interface{}) {
	_log("debug", format, args...)
}

func _log(method string, format string, args ...interface{}) {
	console.Call(method, fmt.Sprintf(format, args...))
}
