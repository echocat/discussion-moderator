package main

import (
	"os"
	"syscall/js"
)

func main() {
	readyState := document.Get("readyState").String()
	if readyState == "interactive" || readyState == "complete" {
		if err := initApplication(); err != nil {
			panic(err)
		}
	} else {
		document.Call("DOMContentLoaded", js.NewCallback(func([]js.Value) {
			if err := initApplication(); err != nil {
				panic(err)
			}
		}), true)
	}
	keepAlive()
}

func initApplication() error {
	if len(os.Args) <= 1 {
		return initIndex()
	} else if os.Args[1] == "project" {
		return initProject()
	} else {
		return initIndex()
	}
}

func initProject() error {
	project, err := newProject()
	if err != nil {
		return err
	}
	doc, _ := loadPdf(project)
	document.Call("addEventListener", "keydown", js.NewCallback(func(args []js.Value) {
		onKeyDownForPdf(doc, args[0])
	}), false)
	return nil
}

func keepAlive() {
	done := make(chan struct{})
	<-done
}

func onKeyDownForPdf(doc *pdf, event js.Value) {
	keyCode := event.Get("keyCode")
	debugf("Button pressed with code: %d", keyCode.Int())

	switch keyCode.Int() {
	case 36:
		doc.goToFirstPage()
	case 35:
		doc.goToLastPage()
	case 34:
		fallthrough
	case 40:
		fallthrough
	case 39:
		doc.movePageBy(1)
	case 8:
		fallthrough
	case 38:
		fallthrough
	case 33:
		fallthrough
	case 37:
		doc.movePageBy(-1)
	case 13: // Return
		fallthrough
	case 32: // Spacebar
		doc.acceptPage()
	case 75: // K
		doc.keepPage()
	}
}
