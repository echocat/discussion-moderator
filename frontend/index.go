package main

import (
	"bytes"
	"html/template"
)

const indexElementsTemplateSource = `
<ul>
	{{range .}}
		<li><a href="/projects/{{ .Id }}">{{ .Name }}</a></li>
	{{end}}
</ul>
`

var (
	indexElementsTemplate *template.Template
)

func init() {
	var err error
	if indexElementsTemplate, err = template.New("indexElements").Parse(indexElementsTemplateSource); err != nil {
		panic(err)
	}
}

func initIndex() error {
	pjs, err := newProjects()
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)
	if err := indexElementsTemplate.Execute(buf, pjs); err != nil {
		return err
	}

	if element, ok := getElementById("index"); ok {
		element.Set("innerHTML", buf.String())
	}

	return nil

}
