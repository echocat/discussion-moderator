package main

import (
	"fmt"
	"syscall/js"
)

const (
	pdfjsSourceRoot = "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.550"
	scale           = 5
)

type pdf struct {
	project       *project
	native        *js.Value
	currentPage   *page
	numberOfPages int
}

func loadPdf(project *project) (instance *pdf, promise js.Value) {
	instance = &pdf{
		project: project,
	}

	pdfjsLib := js.Global().Get("pdfjsLib")
	pdfjsLib.Get("GlobalWorkerOptions").Set("workerSrc", fmt.Sprintf("%s/pdf.worker.js", pdfjsSourceRoot))
	promise = pdfjsLib.Call("getDocument", project.documentFile()).Call("then", js.NewCallback(func(args []js.Value) {
		instance.native = &args[0]
		instance.numberOfPages = instance.native.Get("pdfInfo").Get("numPages").Int()
		instance.goToFirstPage()
	}))

	return
}

func (instance *pdf) movePageBy(difference int) {
	if instance.native == nil {
		warnf("Cannot move page by %d. PDF not already loaded.", difference)
		return
	}
	newPageNumber := difference
	if instance.currentPage != nil {
		newPageNumber = newPageNumber + instance.currentPage.number
	}
	if newPageNumber < 0 || newPageNumber > instance.numberOfPages+1 {
		return
	}
	instance.goToPage(newPageNumber)
}

func (instance *pdf) acceptPage() {
	if instance.currentPage != nil {
		instance.currentPage.accept()
	}
}

func (instance *pdf) keepPage() {
	if instance.currentPage != nil {
		instance.currentPage.keep()
	}
}

func (instance *pdf) goToFirstPage() {
	if instance.native == nil {
		warnf("Cannot move page to first page. PDF not already loaded.")
		return
	}
	instance.goToPage(0)
}

func (instance *pdf) goToLastPage() {
	if instance.native == nil {
		warnf("Cannot move page to last page. PDF not already loaded.")
		return
	}
	if instance.numberOfPages >= 1 {
		instance.goToPage(instance.numberOfPages + 1)
	}
}

func (instance *pdf) goToPage(newPageNumber int) {
	infof("Request to go to page %d.", newPageNumber)
	if instance.native == nil {
		warnf("Cannot go to page %d. PDF not already loaded.", newPageNumber)
		return
	}

	if newPageNumber < 0 || newPageNumber > instance.numberOfPages+1 {
		errorf("Tried to call page %d that does not exist (number of pages: %d).", newPageNumber, instance.numberOfPages)
		return
	}

	instance.displayPageOnElement(newPageNumber, "page-current", true).Call("then", js.NewCallback(func(args []js.Value) {
		instance.displayPageOnElement(newPageNumber-1, "page-previous", false).Call("then", js.NewCallback(func(args []js.Value) {
			instance.displayPageOnElement(newPageNumber+1, "page-next", false)
		}))
	}))
}

func (instance *pdf) displayPageOnElement(number int, onElementId string, targetPage bool) js.Value {
	if number < 1 || number > instance.numberOfPages {
		empty := &page{
			pdf:    instance,
			number: number,
		}
		empty.displayOnElement(onElementId)
		if targetPage {
			if instance.currentPage != nil {
				instance.currentPage.disconnectStatistics()
			}
			instance.currentPage = empty
			instance.currentPage.connectStatistics()
		}
		return timeoutPromise(0)
	}
	return instance.native.Call("getPage", number).Call("then", js.NewCallback(func(args []js.Value) {
		loaded := &page{
			pdf:    instance,
			native: &args[0],
			number: number,
		}
		loaded.displayOnElement(onElementId)
		if targetPage {
			if instance.currentPage != nil {
				instance.currentPage.disconnectStatistics()
			}
			instance.currentPage = loaded
			instance.currentPage.connectStatistics()
		}
	}))
}
