package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type project struct {
	Id                                        string        `json:"id"`
	Name                                      string        `json:"name"`
	MaximumDurationPerPage                    time.Duration `json:"maximumDurationPerPage"`
	WarmUpDurationPerPage                     time.Duration `json:"warmUpDurationPerPage"`
	OnMaximumDurationPerPageReachedMoveToNext bool          `json:"onMaximumDurationPerPageReachedMoveToNext"`
	OnWarmDurationPerPageReachedMoveToNext    bool          `json:"onWarmDurationPerPageReachedMoveToNext"`
}

func newProjects() ([]*project, error) {
	resp, err := http.Get("./projects.json")
	if err != nil {
		return nil, fmt.Errorf("could not open project file (./projects.json): %v", err)
	}
	defer resp.Body.Close()

	var result []*project

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, fmt.Errorf("could not read project file (./projects.json): %v", err)
	}

	return result, nil
}

func newProject() (*project, error) {
	resp, err := http.Get("./properties.json")
	if err != nil {
		return nil, fmt.Errorf("could not open project file (./properties.json): %v", err)
	}
	defer resp.Body.Close()

	result := &project{}

	err = json.NewDecoder(resp.Body).Decode(result)
	if err != nil {
		return nil, fmt.Errorf("could not read project file (./properties.json): %v", err)
	}

	return result, nil
}

func (instance *project) documentFile() string {
	return "./document.pdf"
}

func (instance *project) statisticsFile() string {
	return "./statistics.csv"
}

func (instance *project) reportStatistics(records []string) {
	go func() {
		buf := new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(records)
		if err != nil {
			warnf("could not encode records: %v", err)
			return
		}

		req, err := http.NewRequest("PUT", instance.statisticsFile(), bytes.NewReader(buf.Bytes()))
		if err != nil {
			warnf("could not send statistics to endpoint (%s): %v", instance.statisticsFile(), err)
			return
		}
		req.Header.Set("Content-Type", "application/json")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			warnf("could not send statistics to endpoint (%s): %v", instance.statisticsFile(), err)
			return
		}
		if resp.StatusCode >= http.StatusBadRequest {
			warnf("could not send statistics to endpoint (%s): %d - %s", instance.statisticsFile(), resp.StatusCode, resp.Status)
		}
	}()

}
