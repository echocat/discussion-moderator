package main

import (
	"fmt"
	"syscall/js"
	"time"
)

type page struct {
	pdf                  *pdf
	number               int
	native               *js.Value
	pageDisplayedAt      *time.Time
	statisticsIntervalId *js.Value
	accepted             bool
	keept                bool
}

func (instance *page) displayOnElement(elementId string) {
	if canvas, ok := getElementById(elementId); ok {
		context := canvas.Call("getContext", "2d")

		if instance.native != nil {
			viewport := instance.native.Call("getViewport", scale)
			canvas.Set("width", viewport.Get("width"))
			canvas.Set("height", viewport.Get("height"))

			renderContext := map[string]interface{}{
				"canvasContext": context,
				"viewport":      viewport,
			}
			instance.native.Call("render", renderContext)
		} else {
			width, height := canvas.Get("width"), canvas.Get("height")
			context := canvas.Call("getContext", "2d")
			context.Call("fillRect", 0, 0, width, height)
		}

		now := time.Now()
		instance.pageDisplayedAt = &now
	}
}

func (instance *page) connectStatistics() {
	instance.onUpdateStatistics()
	intervalId := js.Global().Call("setInterval", js.NewCallback(func([]js.Value) {
		instance.onUpdateStatistics()
	}), 250)
	instance.statisticsIntervalId = &intervalId
}

func (instance *page) onUpdateStatistics() {
	now := time.Now()
	displayedSince := now.Sub(*instance.pageDisplayedAt)

	if element, ok := getElementById("readyHint"); ok {
		if instance.native == nil && instance.number <= 0 {
			showElement(element)
		} else {
			hideElement(element)
		}
	}

	if element, ok := getElementById("finishedHint"); ok {
		if instance.native == nil && instance.number > instance.pdf.numberOfPages {
			showElement(element)
		} else {
			hideElement(element)
		}
	}

	if element, ok := getElementById("page-status"); ok {
		parent := element.Get("parentElement")
		if instance.native == nil {
			hideElement(parent)
		} else {
			showElement(parent)
			element.Set("innerText", fmt.Sprintf("%d/%d", instance.number, instance.pdf.numberOfPages))
		}
	}

	if element, ok := getElementById("time-left-for-page-status"); ok {
		parent := element.Get("parentElement")
		if instance.native == nil || instance.keept {
			hideElement(parent)
		} else {
			showElement(parent)
			left := instance.pdf.project.MaximumDurationPerPage - displayedSince
			if left <= time.Duration(0) {
				left = time.Duration(0)
				if instance.pdf.project.OnMaximumDurationPerPageReachedMoveToNext {
					instance.moveToNextPage()
				}
			}
			element.Set("innerText", formatDuration(left))
		}
	}

	if element, ok := getElementById("time-on-page-status"); ok {
		parent := element.Get("parentElement")
		if instance.native == nil || !instance.keept {
			hideElement(parent)
		} else {
			showElement(parent)
			element.Set("innerText", formatDuration(displayedSince))
		}
	}

	if element, ok := getElementById("time-left-for-raise-questions-status"); ok {
		parent := element.Get("parentElement")
		left := instance.pdf.project.WarmUpDurationPerPage - displayedSince
		if instance.native == nil || instance.accepted {
			hideElement(parent)
		} else {
			if left <= time.Duration(0) {
				left = time.Duration(0)
				hideElement(parent)
				if instance.pdf.project.OnWarmDurationPerPageReachedMoveToNext {
					instance.moveToNextPage()
				}
			} else {
				showElement(parent)
			}
			element.Set("innerText", formatDuration(left))
		}
	}
}

func (instance *page) moveToNextPage() {
	instance.disconnectStatistics()
	instance.pdf.movePageBy(1)
}

func (instance *page) disconnectStatistics() {
	now := time.Now()
	displayedSince := now.Sub(*instance.pageDisplayedAt)

	if instance.statisticsIntervalId != nil {
		js.Global().Call("clearInterval", *instance.statisticsIntervalId)
		instance.statisticsIntervalId = nil
	}

	instance.pdf.project.reportStatistics([]string{
		now.Format(time.RFC3339),
		fmt.Sprintf("%d", instance.number),
		formatDuration(displayedSince),
		fmt.Sprintf("%v", instance.accepted),
		fmt.Sprintf("%v", instance.keept),
	})
}

func (instance *page) accept() {
	instance.accepted = true
}

func (instance *page) keep() {
	instance.accepted = true
	instance.keept = true
}
